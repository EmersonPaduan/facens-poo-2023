package exemplo;

// Herança
public class ClienteEspecial extends Cliente {
    private double limite;

    public ClienteEspecial(String nome) {
        super(nome); // super se refere ao construtr da superclasse
    }
    
    public ClienteEspecial(String nome, Telefone telefone) {
        super(nome, telefone); 
    }

    public ClienteEspecial(String nome, Telefone telefone, double limite) {
        super(nome, telefone); 
        this.limite = limite;
    }

    public double getLimite() {
        return limite;
    }

    public void setLimite(double limite) {
        this.limite = limite;
    }

    @Override // sobrescrita de método
    public String toString() {
        if(getTelefone() != null) {
            return "Cliente " + getNome() + ", " + getTelefone().exibir() + " limite " + limite;
        } else {
            return "Cliente " + getNome() + ", telefone não cadastrado";
        }

    }
}
