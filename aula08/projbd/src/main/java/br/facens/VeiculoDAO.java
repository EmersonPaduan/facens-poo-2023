package br.facens;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class VeiculoDAO {
    private static final String URL_BD = "jdbc:mysql://localhost:3306/bd_veiculos";
    private static final String USER_BD = "professor";
    private static final String PASSWORD_BD = "aula";


    // 1. conectar a o BD
    // 2. Enviar  comando SQL para o BD
    // 3. Receber a reposta do BD
    // 4. Fechar a conexão 

    public static boolean inserir(Veiculo novoVeiculo) {
        final String SQL = "insert into veiculos (modelo, ano) values (?, ?)";
        try (
            Connection connection = DriverManager.getConnection(URL_BD, USER_BD, PASSWORD_BD);
            PreparedStatement statement = connection.prepareStatement(SQL);
        ) {
           statement.setString(1, novoVeiculo.getModelo()); 
           statement.setInt(2, novoVeiculo.getAno());
           statement.executeUpdate();

            return true;
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public static Veiculo buscarPorId(int id) {
        final String SQL = "select * from veiculos where id = ?;";
        try (
            Connection connection = DriverManager.getConnection(URL_BD, USER_BD, PASSWORD_BD);
            PreparedStatement statement = connection.prepareStatement(SQL);
        ) {
           statement.setInt(1, id);
           ResultSet resultSet = statement.executeQuery();

            if(resultSet.next()) {
                int idVeiculo = resultSet.getInt("id");
                String modeloVeiculo = resultSet.getString("modelo");
                int anoVeiculo = resultSet.getInt("ano");

                Veiculo veiculo = new Veiculo(idVeiculo, modeloVeiculo, anoVeiculo);
                return veiculo;
            }

            return null;
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }
}
