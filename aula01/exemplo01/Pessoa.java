package exemplo01;

// comentário de linha

/* 
  comentário de bloco
*/

/**
 * Comentário para documentação
 * 
 * Classe Pessoa
 * @author Emerson Paduan
 */

public class Pessoa {

    // atributos
    String nome;

    // métodos
    public void apresentar() {
        System.out.println("Olá! Eu sou " + nome);
    }
}