
public class App {
    public static void main(String[] args) {
        // uma classe abstrata não pode instanciar objetos
        // Figura figura = new Figura(110, 220); // Erro

        Figura circulo = new Circulo(10, 20, 5);
        // System.out.println("Circulo X : " + circulo.getX());

        // circulo.exibir();

        Quadrado quadrado = new Quadrado(1, 2);
        // quadrado.exibir();

        ImprimirTela tela = new ImprimirTela();
        ImprimirPapel papel = new ImprimirPapel();

        tela.exibir(quadrado);
        papel.exibir(circulo);

    }
    
}