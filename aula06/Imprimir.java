public interface Imprimir {
    // public abstract void exibir();
    // todo método é automaticamente public e abstract
    void exibir(Figura figura);

    // nas novas versões do java isso é possível (default)
    default void mostrar() {
        System.out.println();
    }
}
