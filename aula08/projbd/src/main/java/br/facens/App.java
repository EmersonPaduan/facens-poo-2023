package br.facens;

public final class App {

    public static void main(String[] args) {
        // Veiculo v = new Veiculo("Astra", 1990);
        
        // boolean reposta = VeiculoDAO.inserir(v);

        // if(reposta) {
        //     System.out.println("Inserção realizada!");
        // } else {
        //     System.out.println("Falha na inserção.");
        // }

        Veiculo veiculo = VeiculoDAO.buscarPorId(2);

        if(veiculo != null) {
            System.out.println(veiculo);
        }else {
            System.out.println("Veículo não encontrado");
        }

    }
}
