
public class Cliente {

    private String nome;
    private Telefone telefone = null;

    public Cliente() {
        // telefone = new Telefone(0, "xxxx-xxxx");
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Telefone getTelefone() {
        return telefone;
    }

    public void setTelefone(Telefone telefone) {
        this.telefone = telefone;
    }

    
    public String exibir() {
        if(telefone != null) {
            return "Cliente " + nome + ", " + telefone.exibir();
        } else {
            return "Cliente " + nome + ", telefone não cadastrado";
        }

    }

    
}