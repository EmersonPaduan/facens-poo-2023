package exemplo01;

public class Relogio {
    // encapsulamento
    private int hora;
    private int minuto;
    private int segundo;
    
    // construtor default = padrão
    public Relogio() {
        
    }

    // contrutor: inicializar os dados do objeto
    // construtor sobrecarregado
    public Relogio(int novaHora, int novoMinuto, int segundo) {
        setHora(novaHora);
    }

    public Relogio(int novaHora, int novoMinuto) {
        setHora(novaHora);
    }

    public void setHora(int novaHora) {
        if(novaHora >= 0 && novaHora < 24) {
            hora = novaHora;
        }
    }

    public void setMinuto(int minuto) {
        if(minuto >= 0 && minuto < 60) {
            this.minuto = minuto;
        }
    }

    public void setSegundo(int segundo) {
        if(segundo >= 0 && segundo < 60) {
            this.segundo = segundo;
        }
    }

    public int getHora() {
        return hora;
    }

    public int getMinuto() {
        return minuto;
    }

    public int getSegundo() {
        return segundo;
    }

    public String exibir() {
        return hora + ":" + minuto + ":" + segundo;
    }
}