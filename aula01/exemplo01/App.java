package exemplo01;

public class App {
    public static void main(String[] args) {
        
        // alguns tipos primitivos
        int numero;
        double numero2;
        final int VALOR_CONTANTE = 5; // define uma constante

        numero = 7;
        numero = 10;

        // VALOR_CONTANTE = 8; Uma constante não pode mudar  de valor

        Pessoa pessoa1 = new Pessoa();
        Pessoa pessoa2 = new Pessoa();

        System.out.println(pessoa1);
        System.out.println(pessoa2);

        pessoa1.nome = "Emerson";
        pessoa1.apresentar();

        pessoa2.nome = "Paduan";
        pessoa2.apresentar();
        
    }
}
