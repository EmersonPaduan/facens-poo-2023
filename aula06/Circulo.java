public class Circulo extends Figura {
    private int raio;

    public Circulo(int x, int y, int raio) {
        super(x, y);
        this.raio = raio;
    }

    public int getRaio() {
        return raio;
    }

    @Override
    public String toString() {
        return "Circulo [x=" + getX() + ", y=" + getY() + "]";
    }

    @Override
    public void exibir() {
        System.out.println("Circulo: " + getX() + "," + getY());
    }
}
