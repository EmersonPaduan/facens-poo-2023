public abstract class Figura {
    private int x, y;

    public Figura(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }



    // todas as classes que herdam de 'Figura' DEVEM escrever esse método
    public abstract void exibir();
}
