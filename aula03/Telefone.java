public class Telefone {

    private int codigoArea;
    private String number;

    public Telefone(int codigoArea, String number) {
        this.codigoArea = codigoArea;
        this.number = number;
    }

    public String exibir() {
        return "Telefone (" + codigoArea + ") " + number;
    }

    public void alterarTelefone(int codigoArea, String number) {
        this.codigoArea = codigoArea;
        this.number = number;
    }

}
