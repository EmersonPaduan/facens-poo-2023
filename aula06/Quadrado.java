public class Quadrado extends Figura {

    public Quadrado(int x, int y) {
        super(x, y);
    }

    @Override
    public String toString() {
        return "Quadrado [x=" + getX() + ", y=" + getY() + "]";
    }

    @Override
    public void exibir() {
        System.out.println("Quadrado: " + getX() + "," + getY());
    }

}
