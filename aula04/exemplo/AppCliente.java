package exemplo;

public class AppCliente {
    public static void main(String[] args) {
        Telefone tel = new Telefone(11, "98765-4321");
        Cliente cliente = new Cliente("Alessandra", tel);

        Cliente especial = new ClienteEspecial("Gustavo", tel, 1000);
        
        Cliente vetorClientes[] = new Cliente[2];
        vetorClientes[0] = cliente;
        vetorClientes[1] = especial;

        // for (int i = 0; i < vetorClientes.length; i++) {
        //     System.out.println(vetorClientes[i]);
        // }

        for (Cliente c : vetorClientes) { // foreach: para cada Cliente c da estrutura vetor faça
            System.out.println(c);
        }
    }
}
