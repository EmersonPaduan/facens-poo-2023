public class ImprimirTela implements Imprimir {

    @Override
    public void exibir(Figura figura) {
        System.out.println("Exibindo na tela");
        System.out.println(figura);
    }
    
}
