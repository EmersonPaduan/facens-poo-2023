// Herança
public class ClienteEspecial extends Cliente {
    private double limite;

    public double getLimite() {
        return limite;
    }

    public void setLimite(double limite) {
        this.limite = limite;
    }

    @Override
    public String exibir() {
        if(getTelefone() != null) {
            return "Cliente " + getNome() + ", " + getTelefone().exibir() + " limite " + limite;
        } else {
            return "Cliente " + getNome() + ", telefone não cadastrado";
        }

    }
}
