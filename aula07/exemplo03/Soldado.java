package exemplo03;

public class Soldado {
    static int quantidade = 0;
    
    public Soldado(){
        quantidade++;
    }

    public void atacar() {
        if(quantidade > 1) {
            System.out.println("Atacando!!!");
        } else {
            System.out.println("...");
        }
    }
}
