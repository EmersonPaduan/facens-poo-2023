package exemplo;

public class Cliente { // extends Object

    private String nome;
    private Telefone telefone = null;

    public Cliente(){
        // nome = "Sem nome";
        // telefone = new Telefone(0, "xxxx-xxxx");
    }

    public Cliente(String nome) {
        this.nome = nome;
        telefone = new Telefone(0, "xxxx-xxxx");
    }

    public Cliente(String nome, Telefone telefone) {
        this.nome = nome;
        this.telefone = telefone;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Telefone getTelefone() {
        return telefone;
    }

    public void setTelefone(Telefone telefone) {
        this.telefone = telefone;
    }

    @Override
    public String toString() {
        if(telefone != null) {
            return "Cliente " + nome + ", " + telefone.exibir();
        } else {
            return "Cliente " + nome + ", telefone não cadastrado";
        }

    }

    
}