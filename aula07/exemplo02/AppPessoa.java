package exemplo02;

public class AppPessoa {
    public static void main(String[] args) {
        Pessoa p1 = new Pessoa();
        Pessoa p2 = new Pessoa();

        // p1.idade = 10;
        // p2.idade = 20;
        // Pessoa.idade = 10;
        Pessoa.idade = 20;

        // System.out.println("Idades: " + p1.idade + " - " + p2.idade);
        // System.out.println("Idades: " + p1.getIdade() + " - " + p2.getIdade());
        // System.out.println("Idades: " + Pessoa.idade + " - " + Pessoa.idade);
        System.out.println("Idades: " + Pessoa.getIdade() + " - " + Pessoa.getIdade());

        
    }
}
