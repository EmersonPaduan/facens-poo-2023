public class AppCliente {
    public static void main(String[] args) {
        Cliente cliente = null;
        Cliente cliente2 = null;

        cliente = new Cliente();
        cliente2 = new Cliente();

        cliente.setNome("Alessandra");
        cliente2.setNome("Ricardo");

        Telefone tel = new Telefone(11, "98765-4321");
        cliente.setTelefone(tel);

        cliente.getTelefone().alterarTelefone(12, "1111-1111");

        System.out.println(cliente.exibir());
        System.out.println(cliente2.exibir());

        ClienteEspecial especial = new ClienteEspecial();
        especial.setNome("Gustavo");
        especial.setTelefone(tel);
        especial.setLimite(1000);
        System.out.println(especial.exibir());

    }
}
